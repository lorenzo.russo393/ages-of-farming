using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody rb;
    NavMeshAgent agent;

    [SerializeField] float speed;

    [SerializeField] bool isMoving = false;

    Vector3 newPos;

    int touches = 0;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        ClickPoint();
    }

    private void ClickPoint()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray raycast = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(raycast, out hit))
            {
                newPos = hit.point;
                newPos.y = 1;

                isMoving = true;
                StartCoroutine(GoToPoint());   
            }
        }
    }

    private IEnumerator GoToPoint()
    {
        while (isMoving)
        {
            Vector3 posToReach = newPos;

            agent.SetDestination(posToReach);

            if (Vector3.Distance(transform.position, posToReach) <= 0.1f)
            {
                Debug.Log("de");
                isMoving = false;
            }
            else
            {
                yield return null;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Debug.DrawRay(transform.position, newPos, Color.red);
    }
}
