using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ClickManager : MonoBehaviour
{
    public static event Action OnClickLeft;
    public static event Action OnClickRight;
    public static event Action OnScroll;

    private void Update()
    {
        MouseInputCheck();      
    }

    private void MouseInputCheck()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnClickLeft?.Invoke();
        }

        if (Input.GetMouseButtonDown(1))
        {
            OnClickRight?.Invoke();
        }

        if (Input.mouseScrollDelta != null)
        {
            OnScroll?.Invoke();
        }
    }
}
