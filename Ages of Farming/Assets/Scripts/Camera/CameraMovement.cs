using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using UnityEngine.UIElements;
using static UnityEditor.PlayerSettings;

public class CameraMovement : MonoBehaviour
{
    [Header("Move proprieties")]
    [SerializeField] float sensibility = 2;
    [SerializeField] float movementSpeed = 1;
    [SerializeField] float momentumFactor = 0.8f;

    [Header("Zoom proprieties")]
    [SerializeField] float zoomSpeed = 2;

    private void LateUpdate()
    {
        Move();
        Zoom();
    }

    private void Move()
    {
        if (Input.GetMouseButton(1))
        {
            float x = Input.GetAxis("Mouse X");
            float z = Input.GetAxis("Mouse Y");


            Vector3 move = new Vector3(x, 0, z) * sensibility;

            Vector3 final = transform.position - move;
            move.Normalize();

            transform.position = Vector3.Lerp(transform.position, final, movementSpeed * Time.deltaTime);

            //transform.position += move * movementSpeed * Time.deltaTime;
        }
    }
    private void Zoom()
    {
        float scroll = 0;
        scroll = Input.GetAxis("Mouse ScrollWheel");

        if (scroll != 0)
        {
            if (Mathf.Abs(scroll) > 0)
            {
                Camera.main.fieldOfView -= scroll * zoomSpeed;
            }
            else if (Mathf.Abs(scroll) < 0)
            {
                Camera.main.fieldOfView += scroll * zoomSpeed;
            }
        }
    }
}
